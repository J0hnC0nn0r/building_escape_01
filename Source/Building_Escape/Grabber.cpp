// Fill out your copyright notice in the Description page of Project Settings.


#include "Grabber.h"
#include "DrawDebugHelpers.h"
#include "CollisionQueryParams.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"

#define OUT // For readability only

// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();

	FindPhysicsHandle();
	SetupInputComponent();
}

// Checking for Physics Handle Component
void UGrabber::FindPhysicsHandle()
{
	PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	if (PhysicsHandle)
	{
		// Physics is found.
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("There is no UPhysicsHandleComponent on the object: %s"), *GetOwner()->GetName());
	}
}

// Checking for Input Component
void UGrabber::SetupInputComponent()
{
	InputComponent = GetOwner()->FindComponentByClass<UInputComponent>();
	if (InputComponent)
	{
		// Input found
		InputComponent->BindAction("Grab", IE_Pressed, this, &UGrabber::Grab);
		InputComponent->BindAction("Grab", IE_Released, this, &UGrabber::Release);
	}
}

void UGrabber::Grab()
{
	// Get Players viewpoint
	FVector PlayerViewPortLocation;
	FRotator PlayerViewPortRotation;

	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
		OUT PlayerViewPortLocation,
		OUT PlayerViewPortRotation
	);

	// Draw Line from player showing the reach
	LineTraceEnd = PlayerViewPortLocation + PlayerViewPortRotation.Vector() * Reach;
	UE_LOG(LogTemp, Warning, TEXT("Grabber Pressed..."));

	FHitResult HitResult = GetFirstPhysicsBodyInReach();
	UPrimitiveComponent* ComponentToGrab = HitResult.GetComponent();
	// Try and reach any actors with physics body collision channel set

	// If we hit something then attach the physics handle
	// ToDo attach physics handle
	if (HitResult.GetActor())
	{
		PhysicsHandle->GrabComponentAtLocation
		(
			ComponentToGrab,
			NAME_None,
			LineTraceEnd
		);
	}
}

void UGrabber::Release()
{
	UE_LOG(LogTemp, Warning, TEXT("Grabber Released..."));
	// Get Players viewpoint
	FVector PlayerViewPortLocation;
	FRotator PlayerViewPortRotation;

	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
		OUT PlayerViewPortLocation,
		OUT PlayerViewPortRotation
	);

	// Draw Line from player showing the reach
	LineTraceEnd = PlayerViewPortLocation + PlayerViewPortRotation.Vector() * Reach;
	// ToDo remove/release the physics handle.
	PhysicsHandle->ReleaseComponent();
}

// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	// Get Players viewpoint
	FVector PlayerViewPortLocation;
	FRotator PlayerViewPortRotation;

	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
		OUT PlayerViewPortLocation,
		OUT PlayerViewPortRotation
	);

	// Draw Line from player showing the reach
	LineTraceEnd = PlayerViewPortLocation + PlayerViewPortRotation.Vector() * Reach;

	// If the physic handle is attached
	if (PhysicsHandle->GrabbedComponent)
	{
		// Move the object we are holding
		PhysicsHandle->SetTargetLocation(LineTraceEnd);
	}
		
}

FHitResult UGrabber::GetFirstPhysicsBodyInReach()
{
	// Get Players viewpoint
	FVector PlayerViewPortLocation;
	FRotator PlayerViewPortRotation;

	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
		OUT PlayerViewPortLocation,
		OUT PlayerViewPortRotation
	);

	// Draw Line from player showing the reach
	LineTraceEnd = PlayerViewPortLocation + PlayerViewPortRotation.Vector() * Reach;
	/* 
	DrawDebugLine(
		GetWorld(),
		PlayerViewPortLocation,
		LineTraceEnd,
		FColor(0, 255, 0),
		false,
		0.f,
		0,
		5.f
	);
	*/

	FHitResult Hit;
	// Ray-cast out to a certain distance (Reach)
	FCollisionQueryParams TraceParams(FName(TEXT("")), false, GetOwner());

	GetWorld()->LineTraceSingleByObjectType(
		OUT Hit,
		PlayerViewPortLocation,
		LineTraceEnd,
		FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody),
		TraceParams
	);

	// See what it hits
	AActor* ActorHit = Hit.GetActor();
	if (ActorHit)
	{
		UE_LOG(LogTemp, Warning, TEXT("Actor: %s was Hit!"), *ActorHit->GetName());
	}

	return Hit;
}